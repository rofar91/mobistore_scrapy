import scrapy


class MobistoreSpider(scrapy.Spider):
    name = 'mobistore'
    allowed_domains = ['mobistore.by']
    start_urls = ['https://mobistore.by/mobilnye-telefony']

    def parse(self, response):
        for item in response.xpath('.//div[@class="product text-center"]'):
            yield {
                'model': item.xpath('.//a[@class="product-name"]/span/text()').get(),
                'price': item.xpath('.//span[@class="price"]/span/text()').get(),
            }

        next_page = response.xpath('//a[@class="next_page_link"]/@href').get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
