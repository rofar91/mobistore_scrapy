# -*- coding: utf-8 -*-
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector


class TechnopointSpider(CrawlSpider):
    name = 'technopoint'
    allowed_domains = ['technopoint.ru']
    start_urls = ['https://technopoint.ru/catalog/17a8a01d16404e77/smartfony/']

    rules = (
        Rule(
            LinkExtractor(
                restrict_xpaths=['.//a[@class="ui-link"]'],
                allow=r'https:\/\/technopoint\.ru\/product[^S]*'
            ),
            callback='parse_item'
        ),
    )

    def parse_item(self, response):
        for item in response.xpath('.//div[@class="container"]'):
            yield {
                'model': item.xpath('.//h1[@class="page-title price-item-title"]/text()').get(),
                'price': item.xpath('.//div[@class="price_g"]/span/text()').get(),
                'url': response.url,
            }

        # next_page = response.xpath('//a[@class="next_page_link"]/@href').get()
        # if next_page is not None:
        #     next_page = response.urljoin(next_page)
        #     yield scrapy.Request(next_page, callback=self.parse)
